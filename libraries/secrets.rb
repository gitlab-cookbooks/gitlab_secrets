# base module
module Decryptor
  # base class for backends

  # Implements getting secrets from chef-vault
  class GitlabChefVault
    def initialize(path, key, node)
      @path = path
      @key = key.nil? ? node.chef_environment : key
      @node = node
      Chef::Log.info("gitlab_secrets: BE: 'chef_vault', path: '#{@path}', key:'#{@key}'")
    end

    def get
      require 'chef-vault'
      if ChefVault::Item.vault?(@path, @key)
        Hash(ChefVault::Item.load(@path, @key))
      else
        Chef::Log.warn("This is not a vault, will try to load the #{@key} in the #{@path} databag.")
        Hash(Chef::DataBagItem.load(@path, @key))
      end
    end
  end
  # rubocop:enable LineLength

  # Implements getting secrets from KMS encrypted file stored in GCS bucket
  class GitlabGkms
    include Chef::Mixin::ShellOut
    def initialize(path, key, node)
      @path = path
      @key = key
      @node = node
      verify_path(@path)
      verify_key(@key)
      Chef::Log.info("gitlab_secrets: BE: 'gkms', path: '#{@path}', key:'#{@key}'")
    end

    def verify_path(path)
      raise "Path must contain a \"path\" and an \"item\", you passed: #{path}" unless path.key?('path') && path.key?('item')
    end

    def verify_key(key)
      raise "Key must contain a \"ring\", a \"key\" and a \"location\", you passed: #{key}" unless key.key?('key') && key.key?('ring') && key.key?('location')
    end

    def get
      bucketpath = @path['path']
      bucketitem = @path['item']
      keyring    = @key['ring']
      key        = @key['key']
      location   = @key['location']
      project    = @key['project']
      # encrypt string with:
      # gcloud kms encrypt
      #  --keyring=$KEYRING-TO-USE
      #  --key=$KEY-TO-ENCRYPT
      #  --location=$REGION
      #  --plaintext-file=$PLAINTEXT-FILE-TO-ENCRYPT
      #  --ciphertext-file=$ENCRYPTED-OUTPUT
      # https://cloud.google.com/storage/docs/gsutil/commands/cp
      # https://cloud.google.com/sdk/gcloud/reference/kms/decrypt
      # where "-" is used as a place holder for stdin/stdout

      # We finish the pipeline with a "zcat -f" which will optionally decompress the json
      # if it was previously compressed before it was encrypted
      decryptsecret = "gsutil cp gs://#{bucketpath}/#{bucketitem} - | gcloud" \
                      "#{project.nil? ? '' : " --project #{project}"} " \
                      'kms decrypt ' \
                      "--keyring=#{keyring} " \
                      "--key=#{key} " \
                      "--location=#{location} " \
                      '--plaintext-file=- ' \
                      '--ciphertext-file=- | zcat -f'
      require 'json'
      JSON.parse(shell_out!(decryptsecret).stdout)
    end
  end

  # Implements getting secrets from Google Secret Manager
  class GitlabGsm
    include Chef::Mixin::ShellOut
    def initialize(path, key, node)
      @path = path
      @key = key
      @node = node
      verify_path(@path)
      Chef::Log.info("gitlab_secrets: BE: 'gsm', path: '#{@path}', key:'#{@key}'")
    end

    def verify_path(path)
      raise "Path must contain a \"name\" and a \"project\", you passed: #{path}" unless path.key?('name') && path.key?('project')
    end

    def get
      secretname = @path['name']
      secretproject = @path['project']
      decryptsecret = "gcloud --project #{secretproject} " \
                      'secrets versions access ' \
                      "--secret #{secretname} latest"
      require 'json'
      JSON.parse(shell_out!(decryptsecret).stdout)
    end
  end

  class GitlabHashicorpVault
    def initialize(path, key, node)
      @path = path
      @key = key.is_a?(Hash) ? key : {}
      @node = node
      verify_path(@path)
      verify_key(@key) unless @key.empty?
      Chef::Log.info("gitlab_secrets: BE: 'hashicorp_vault', path: '#{@path}', key:'#{@key}'")

      raise "No role defined in node['gitlab_secrets']['hashicorp_vault']['role']. Cannot continue without a role" unless @node['gitlab_secrets']['hashicorp_vault']['role']
    end

    def verify_path(path)
      raise "Path must contain \"path\" and \"mount\". You passed: #{path}" unless path.key?('path') && path.key?('mount')
    end

    def verify_key(key)
      raise "Key must contain \"key\". You passed: #{key}" unless key.key?('key')
      raise "Key must be a string. You passed: #{key['key']}" unless key['key'].is_a?(String)
    end

    def get
      require 'vault'

      Chef::Log.info("gitlab_secrets: no key specified - getting all attributes for path '#{@path['mount']}/#{@path['path']}'") if @key.empty?

      unless Vault.token
        Chef::Log.info('gitlab_secrets: no vault token found - will authenticate to Vault')

        jwt = Chef::HTTP.new('http://metadata', headers: { 'Metadata-Flavor' => 'Google' }).get("/computeMetadata/v1/instance/service-accounts/default/identity?format=full&audience=http://vault/#{URI.escape(@node['gitlab_secrets']['hashicorp_vault']['role'])}")
        Vault.address = @node['gitlab_secrets']['hashicorp_vault']['endpoint']
        Vault.auth.gcp(@node['gitlab_secrets']['hashicorp_vault']['role'], jwt)
      end

      data = Vault.with_retries(Vault::HTTPConnectionError) do
        Vault.kv(@path['mount']).read(@path['path'])
      end

      raise "Unable to retrieve secret '#{@path['mount']}/#{@path['path']}' using role '#{@node['gitlab_secrets']['hashicorp_vault']['role']}' from Vault endpoint '#{Vault.address}'" unless data

      secrets = data.data.stringify_keys

      unless @key.empty?
        raise "Key '#{@key['key']}' not found in secret '#{@path['mount']}/#{@path['path']}'" unless secrets.key?(@key['key'])
        return secrets[@key['key']]
      end

      secrets
    end
  end

  # DEFAULT_CLASS   = GitlabChefVault
  SECRET_BACKENDS = {
    'chef_vault'      => GitlabChefVault,
    'gkms'            => GitlabGkms,
    'gsm'             => GitlabGsm,
    'hashicorp_vault' => GitlabHashicorpVault,
  }.freeze unless defined? SECRET_BACKENDS

  def get_secrets(backend, path, key = {})
    #
    # we need to pass the node itself into the
    # concret classes so lets add it here:
    #
    raise "Secrets backend: #{backend} - not found (#{cookbook_name}[#{recipe_name}])" unless SECRET_BACKENDS[backend]
    Chef::Log.info("Getting secrets for #{cookbook_name}[#{recipe_name}]")
    real_secret = SECRET_BACKENDS[backend].new(path, key, node)
    real_secret.get
  end

  def merge_secrets(level)
    node_hash = node.get_deep(*level)
    raise "I did not find a valid hash (type: #{node_hash.class}): #{node_hash}" unless node_hash.is_a? Hash
    raise "There is no `secrets` section in the hash you passed: #{node_hash.keys}" unless node_hash.key?('secrets')
    node_hash['secrets'].each do |name, secret|
      raise "There is not secret section in #{name}, you passed: #{node_hash['secrets']} - The key should be a name, and the value should be a secret configuration" unless secret.is_a? Hash
      Chef::Log.info("mixing secrets from #{name} into #{node_hash.keys} for #{cookbook_name}[#{recipe_name}]")
      node_hash = node_hash.deep_merge(
        get_secrets(secret['backend'],
                    secret['path'],
                    secret['key']).get_deep(*level) || {})
    end
    (node_hash || {}).to_hash
  end
end

Chef::Recipe.send(:include, Decryptor)
Chef::Resource.send(:include, Decryptor)
Chef::Provider.send(:include, Decryptor)

private

class ::Hash
  def stringify_keys
    h = map do |k, v|
      v_str = if v.instance_of? Hash
                v.stringify_keys
              else
                v
              end

      [k.to_s, v_str]
    end
    Hash[h]
  end

  # flattens a list into a hash-like key
  # so we can retrieve nested values:
  #
  # node.get_deep['a','b']
  # results in node['a']['b'] being returned
  #
  def get_deep(*fields)
    fields.inject(self) { |acc, elem| acc[elem] if acc }
  end

  # Merges 2 hashes:
  #
  #   hash1.deep_merge(hash2)
  #
  #   will merge hash1 and hash2.
  #   If a value is in both the values
  #   of hash1 to be overwritten by hash2
  #
  def deep_merge(other_hash, &block)
    dup.deep_merge!(other_hash, &block)
  end

  # Cribbed from Ruby on Rails: http://apidock.com/rails/Hash/deep_merge%21
  # File activesupport/lib/active_support/core_ext/hash/deep_merge.rb, line 21
  def deep_merge!(other_hash, &block)
    other_hash.each_pair do |current_key, other_value|
      this_value = self[current_key]

      self[current_key] = if this_value.is_a?(Hash) && other_value.is_a?(Hash)
                            this_value.deep_merge(other_value, &block)
                          else
                            # rubocop:disable IfInsideElse
                            if block_given? && key?(current_key)
                              yield(current_key, this_value, other_value)
                            else
                              other_value
                            end
                          end
    end
    self
  end
end
