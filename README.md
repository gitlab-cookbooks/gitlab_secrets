[![build status](https://gitlab.com/gitlab-cookbooks/gitlab_secrets/badges/master/build.svg)](https://gitlab.com/gitlab-cookbooks/gitlab_secrets/commits/master)
[![coverage report](https://gitlab.com/gitlab-cookbooks/gitlab_secrets/badges/master/coverage.svg)](https://gitlab.com/gitlab-cookbooks/gitlab_secrets/commits/master)

# gitlab_secrets

This is gitlab_secrets cookbook to add library functions to your cookbooks

## Using

include the `default` recipe in your run_list or add a `depends gitlab_secrets` to your cookbook.
This will offer you two global methods:

* get_secrets
* merge_secrets

These methods both can take a variety of secret back-ends. Currently, however only

* chef_vault
* gkms
* gsm

are setup.

### get_secrets(backend,path,key)

`get_secrets` will simply lookup the given path/key and return the hash it finds for either _chef_vault_  or _gkms_ backends.

#### chef_vault backend

```
foo = get_secrets('chef_vault','vault_name','vault_item')
```
will try to look for a vault (or data_bag for test use) with the name `vault_name` and retrieve the `vault_item` there-in. Similar to `GitLab::Vault`, if no `vault_item` is given, `node.chef_environment` will be used.

```
my_recipe_secrets = get_secrets('chef_vault','gitlab-omnibus')
```

e.g. running this code on a `production` node will result in the `prd` item in the `gitlab-omnibus` vault to be retrieved and the entire hash to be set the `my_recipe_secrets`.

#### gkms backend
GKMS use is twofold:

* retrieve the encrypted object from google storage
* decrypt it

Thus the `path` pertains to the location of the encrypted object, and the `key` to the decryption of the object.

in gkms a `path` is a hash of:

```
"path":{
  "path":"path/to/the/bucket",
  "item":"name_of_the_item_to_be_retrieved.enc"
}
```
while the `key` is a hash of:

```
"key":{
  "ring":"key_ring_name",
  "key":"key_to_use",
  "location":"global"
}
```

consider the following snippet:
```
my_path = {'path' => 'gitlab_secrets/gitlab-omnibus', 'item' => 'gprd.enc' }
my_key = {'ring'=> 'gitlab_secrets', 'key' => 'gprd', 'location' => 'global'}
foo = get_secrets('gkms',my_path ,my_key)
```
The following would happen in the background:

```
gsutil cp gs://gitlab_secrets/gitlab_omnibus/gprd.enc - | gcloud kms decrypt
       --keyring=gitlab_secrets
       --key=gprd
       --location=global
       --plaintext-file=-
       --ciphertext-file=- | zcat -f
```
The hash that would have been encrypted would then be set to `foo`.

#### gsm backend
This backend uses [Google Secrets Manager](https://cloud.google.com/secret-manager) to
store an object that contains secrets. The `path` object must contain two attributes,
`name` and `project`.

* `name` is the name of the Google secret to pull the object from
* `project` is the Google Compute Platform project that contains the Google secret in question

consider the following snippet:
```
my_path = {'name' => 'gitlab-omnibus-secrets_gprd', 'project' => 'gitlab-production'}
foo = get_secrets('gsm', my_path, {})
```

This will use the `gcloud` command line tool to run the following command to pull
the secret object
```
gcloud --project gitlab-production secrets versions access --secret gitlab-omnibus-secrets_gprd latest
```
The hash that would have been encrypted would then be set to `foo`.

More information on that specific `gcloud` command can be found at https://cloud.google.com/sdk/gcloud/reference/secrets/versions/access

### merge_secrets(path,to,get,and,merge)
Merge is a continuation of the functionality used by `GitLab:Vault`.

First, it takes the coma seperated list of strings and makes them into a hash key:

e.g.

```
merge_secrets(path,to,get,and,merge)
```

would get translated into a lookup of:
```
node['path']['to']['get']['and']['merge']
```

Second, it will vierify that a `secrets` hash is availible at that location:

```
node['path']['to']['get']['and']['merge']['secrets']
```

Third, it will iterate over each entry in the `secrets` hash calling `get_secrets` with the configured values.

Lastly, it will `deep_merge` the node attributes of `node['path']['to']['get']['and']['merge']` with the secrets it receives.

e.g.

```
attributes_with_secrets = merge_secrets('omnibus')
```

will start by verifying that there are legitimate secrets configured:

```
"omnibus-gitlab": {
      "secrets":{
        "all-the-omnibus-secrets":{
          "backend":"chef_vault",
          "path":"gitlab-cluster-base",
          "key":"prd"
        }
      }
    }
```
next it will iterate over all the keys of `secrets` (in this case only `all-the-omnibus-secrets`) and merge the original values of `node['omnibus-gitlab']` with the content of the `gitlab-cluster-base` vault.

As a result the values in the role may contain hints such as:

```
"omnibus-gitlab": {
  "gitlab_rb":{
    "gitlab_rails":{
      "db_password":"in_the_chef_vault"
    }
  }
}
```
while the end `gitlab.rb` file will contain the contents of the vault:

```
gitlab_rails['db_password'] = "super-secret"
```

## Testing

The Makefile, which holds all the logic, is designed to be the same among all
cookbooks. Just set the comment at the top to include the cookbook name and
you are all set to use the below testing instructions.

### Testing locally

You can run `rspec` or `kitchen` tests directly without using provided
`Makefile`, although you can follow instructions to benefit from it.

1. Install GNU Make (`apt-get install make`). Under OS X you can achieve the
   same by `brew install make`. After this, you can see available targets of
   the Makefile just by running `make` in cookbook directory.

1. Cheat-sheet overview of current targets:

 * `make gems`: install latest version of required gems into directory,
   specified by environmental variable `BUNDLE_PATH`. By default it is set to
   the same directory as on CI, `.bundle`, in the same directory as Makefile
   is located.

 * `make check`: find all `*.rb` files in the current directory, excluding ones
   in `BUNDLE_PATH`, and check them with cookstyle.

 * `make rspec`: the above, plus run all the rspec tests. You can use
   `bundle exec rspec -f d` to skip the lint step, but it is required on CI
   anyways, so rather please fix it early ;)

 * `make kitchen`: calculate the number of suites in `.kitchen.do.yml`, and
   run all integration tests, using the calculated number as a `concurrency`
   parameter. In order to this locally by default, copy the example kitchen
   config to your local one: `cp .kitchen.do.yml .kitchen.local.yml`, or
   export environmental variable: `export KITCHEN_YAML=".kitchen.do.yml"`

   *Note* that `.kitchen.yml` is left as a default Vagrant setup and is not
   used by Makefile.

1. In order to use DigitalOcean for integration testing locally, by using
   `make kitchen` or running `bundle exec kitchen test --destroy=always`,
   export the following variables according to the
   [kitchen-digitalocean](https://github.com/test-kitchen/kitchen-digitalocean)
   documentation:
  * `DIGITALOCEAN_ACCESS_TOKEN`
  * `DIGITALOCEAN_SSH_KEY_IDS`

### on CI

Alternatively, you can just push to your branch and let CI handle the testing.
To setup it, add the `DIGITALOCEAN_ACCESS_TOKEN` secret variable under your
project settings, `make kitchen` target will:
 * detect the CI environment
 * generate ephemeral SSH ed25519 keypair
 * register them on DigitalOcean
 * export the resulting key as `DIGITALOCEAN_SSH_KEY_IDS` environment variable
 * run the kitchen test
 * clean up the ephemeral key from DigitalOcean after pipeline is done

See `.gitlab-ci.yml` for details, but the overall goal is to have only
`make rspec` and `make kitchen` in it, and cache `$BUNDLE_PATH` for speed.

Since `make check` is a prerequisite for `make rspec`, current CI configuration
basically enforces all of the following to succeed, in defined order, for a
pipeline to pass:
 * Rubocop should be happy with every ruby file and exit with dcode zero,
   without any warnings or errors.
 * Rspec tests must all pass.
 * Integration tests must all pass.
