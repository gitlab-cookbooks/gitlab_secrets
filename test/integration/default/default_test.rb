# InSpec tests for recipe template::default

control 'general-checks' do
  impact 1.0
  title 'General tests for gitlab_secrets cookbook'
  desc '
    This control ensures that:
      * there is no duplicates in /etc/group'

  describe file('/tmp/test') do
    its('content') { should match /^test-value/ }
  end
  describe file('/tmp/merge-test') do
    its('content') { should match /^{"test-key"=>"from-data-bag", "recipe-key"=>"from-recipe", "data-bag-key"=>"from-data-bag"}/ }
  end
  describe file('/etc/apt/sources.list.d/google-cloud-sdk.list') do
    its('content') { should match(%r{^deb\s*"?https\:\/\/packages.cloud.google.com\/apt"?\s*cloud-sdk\s*main}) }
  end
end
