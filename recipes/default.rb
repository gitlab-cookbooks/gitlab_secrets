
#
# Cookbook Name:: template
# Recipe:: default
# License:: MIT
#
# Copyright 2016, GitLab Inc.
chef_gem 'chef-vault' do # ~FC009
  compile_time true if Chef::Resource::ChefGem.method_defined?(:compile_time)
end

# hashicorp_vault
chef_gem 'vault' do # ~FC009
  compile_time true if Chef::Resource::ChefGem.method_defined?(:compile_time)
end

apt_repository 'google-cloud-sdk' do
  uri           node['gitlab_secrets']['google_cloud_sdk_uri']
  distribution  node['gitlab_secrets']['google_cloud_sdk_dist']
  components    node['gitlab_secrets']['google_cloud_sdk_components']
  key           node['gitlab_secrets']['google_cloud_sdk_key']
end
