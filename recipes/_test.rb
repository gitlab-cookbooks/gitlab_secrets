
#
# Cookbook Name:: template
# Recipe:: default
# License:: MIT
#
# Copyright 2016, GitLab Inc.

include_recipe 'gitlab_secrets::default'

file '/tmp/test' do
  content get_secrets(node['secrets']['backend'],
                      node['secrets']['path'],
                      node['secrets']['key'])['test-key']
end

file '/tmp/merge-test' do
  content merge_secrets('merge-me')['test-keys'].to_s
end
