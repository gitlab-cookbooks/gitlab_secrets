default['gitlab_secrets']['backend'] = 'chef_vault'

default['gitlab_secrets']['google_cloud_sdk_uri'] = 'https://packages.cloud.google.com/apt'
default['gitlab_secrets']['google_cloud_sdk_dist'] = 'cloud-sdk'
default['gitlab_secrets']['google_cloud_sdk_components'] = ['main']
default['gitlab_secrets']['google_cloud_sdk_key'] = 'https://packages.cloud.google.com/apt/doc/apt-key.gpg'

default['gitlab_secrets']['hashicorp_vault']['endpoint'] = 'https://vault.ops.gke.gitlab.net'
