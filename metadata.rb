name             'gitlab_secrets'
maintainer       'GitLab Inc.'
maintainer_email 'jtevnan@gitlab.com'
license          'MIT'
description      'Cookbook template for GitLab cookbooks'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.1.0'
chef_version     '>= 12.1' if respond_to?(:chef_version)
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_secrets/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_secrets'

supports 'ubuntu', '= 16.04'
depends 'chef-vault'
