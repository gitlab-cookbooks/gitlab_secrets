# Cookbook:: template
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'

describe 'gitlab_secrets::_test' do
  context 'HASHICORP VAULT: default execution' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab_secrets']['hashicorp_vault']['role'] = 'role123'
        node.normal['secrets'] = {
          'backend' => 'hashicorp_vault',
          'path' => {
            'path'  => 'test-secret-chef-env-zero-bucket/bar',
            'mount' => 'mount',
          },
        }
        node.normal['merge-me'] = {
          'secrets' => {
            'all-the-secrets' => {
              'backend' => 'hashicorp_vault',
              'path' => {
                'path' => 'test-secret-chef-env-zero-bucket',
                'mount' => 'mount',
              },
            },
          },
          'test-keys' => {
            'test-key' => 'from-recipe',
            'recipe-key' => 'from-recipe',
          },
        }
      end.converge(described_recipe)
    end

    let(:kv) do
      kv = double(Vault::KV)
      allow(Vault::KV).to receive(:new).and_return(kv)

      kv
    end

    before do
      require 'vault'

      http = double(Chef::HTTP)
      allow(Chef::HTTP).to receive(:new).and_return(http)
      allow(http).to receive(:get).with(instance_of(String)).and_return('jwt123')

      allow(Vault.auth).to receive(:gcp).with(instance_of(String), instance_of(String))
    end

    it 'converges successfully' do
      allow(kv).to receive(:read)
        .with(instance_of(String))
        .and_return(Vault::Secret.decode(data: {}))

      expect { chef_run }.to_not raise_error
    end

    it 'get_secrets() works correctly' do
      allow(kv).to receive(:read)
        .with(instance_of(String))
        .and_return(Vault::Secret.decode(data: { 'test-key' => 'testvalue' }))

      expect(chef_run).to create_file('/tmp/test').with(
        content: /^testvalue/
      )
    end

    it 'merge_secrets() works correctly' do
      allow(kv).to receive(:read)
        .with(instance_of(String))
        .and_return(
          Vault::Secret.decode(
            data: {
              'merge-me' => {
                'test-keys' => {
                  'test-key' => 'from-hashicorp-vault',
                  'additional-key' => 'from-vault',
                },
              },
            }
          )
        )

      expect(chef_run).to create_file('/tmp/merge-test').with(
        content: '{"test-key"=>"from-hashicorp-vault", "recipe-key"=>"from-recipe", "additional-key"=>"from-vault"}'
      )
    end
  end
end
