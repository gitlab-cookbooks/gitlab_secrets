# Cookbook:: template
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'

describe 'gitlab_secrets::_test' do
  context 'HASHICORP VAULT: missing path validation' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab_secrets']['hashicorp_vault']['role'] = 'role123'
        node.normal['secrets'] = {
          'backend' => 'hashicorp_vault',
          'path' => {
            'mount' => 'mount',
          },
        }
      end.converge(described_recipe)
    end

    it 'should raise an error as the "path" key is missing' do
      expect { chef_run }.to raise_error(RuntimeError, /^Path must contain/)
    end
  end

  context 'HASHICORP VAULT: missing mount validation' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab_secrets']['hashicorp_vault']['role'] = 'role123'
        node.normal['secrets'] = {
          'backend' => 'hashicorp_vault',
          'path' => {
            'path' => 'test-secret-chef-env-zero-bucket/bar',
          },
        }
      end.converge(described_recipe)
    end

    it 'should raise an error as the "mount" key is missing' do
      expect { chef_run }.to raise_error(RuntimeError, /^Path must contain/)
    end
  end

  context 'HASHICORP VAULT: missing role validation' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['secrets'] = {
          'backend' => 'hashicorp_vault',
          'path' => {
            'path'  => 'test-secret-chef-env-zero-bucket/bar',
            'mount' => 'mount',
          },
        }
      end.converge(described_recipe)
    end

    it 'should raise an error as the role is missing' do
      expect { chef_run }.to raise_error(RuntimeError, /^No role defined/)
    end
  end

  context 'HASHICORP VAULT: missing key validation' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab_secrets']['hashicorp_vault']['role'] = 'role123'
        node.normal['secrets'] = {
          'backend' => 'hashicorp_vault',
          'path' => {
            'path'  => 'test-secret-chef-env-zero-bucket/bar',
            'mount' => 'mount',
          },
          'key' => {
            'wrong_key' => 'foo',
          },
        }
      end.converge(described_recipe)
    end

    it 'should raise an error as the "key" is missing' do
      expect { chef_run }.to raise_error(RuntimeError, /^Key must contain/)
    end
  end
end
