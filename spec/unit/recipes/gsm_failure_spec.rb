# Cookbook:: template
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'

describe 'gitlab_secrets::_test' do
  let(:shellout) do
    double(run_command: nil,
           error!: nil,
           stdout: '',
           stderr: '',
           exitstatus: 0,
           live_stream: '')
  end

  before do
    allow(Mixlib::ShellOut).to receive(:new).and_return(shellout)
  end

  context 'GKMS: path verification error' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['merge-me'] = {
          'secrets' => {
            'all-the-secrets' => {
              'backend' => 'gsm',
              'path' => {
                'wrong_path' => 'test-secret-chef_env-zero',
                'item' => 'env-zero',
              },
            },
          },
          'test-keys' => {
            'test-key' => 'from-recipe',
            'recipe-key' => 'from-recipe',
          },
        }
        node.normal['secrets'] = {
          'backend' => 'gsm',
          'path' => {
            'path' => 'test-secret-chef_env-zero',
            'item' => 'env-zero',
          },
        }
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      allow(shellout).to receive(:stdout)
        .and_return('{"merge-me":{"test-keys":"testvalue"}}')

      expect { chef_run }.to raise_error
    end
  end
end
