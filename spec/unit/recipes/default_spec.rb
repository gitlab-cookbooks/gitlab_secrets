# Cookbook:: template
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'

describe 'gitlab_secrets::_test' do
  context 'default execution failure' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['secrets'] = {
          'backend' => 'unreal-backend',
          'vault' => 'vault',
          'item' => 'item',
        }
      end.converge(described_recipe)
    end

    it 'it fails to converge' do
      expect { chef_run }.to raise_error(RuntimeError)
    end
  end
end
