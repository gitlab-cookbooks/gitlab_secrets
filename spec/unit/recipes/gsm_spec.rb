# Cookbook:: template
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'

describe 'gitlab_secrets::_test' do
  let(:shellout) do
    double(run_command: nil,
           error!: nil,
           stdout: '',
           stderr: '',
           exitstatus: 0,
           live_stream: '')
  end

  before do
    allow(Mixlib::ShellOut).to receive(:new).and_return(shellout)
  end

  context 'GSM: default execution' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['merge-me'] = {
          'secrets' => {
            'all-the-secrets' => {
              'backend' => 'gsm',
              'path' => {
                'name' => 'test-secret-chef_env-zero',
                'project' => 'env-zero',
              },
            },
          },
          'test-keys' => {
            'test-key' => 'from-recipe',
            'recipe-key' => 'from-recipe',
          },
        }
        node.normal['secrets'] = {
          'backend' => 'gsm',
          'path' => {
            'name' => 'test-secret-chef_env-zero',
            'project' => 'env-zero',
          },
        }
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      allow(shellout).to receive(:stdout)
        .and_return('{"merge-me":{"test-keys":"testvalue"}}')

      expect { chef_run }.to_not raise_error
    end

    it 'creates the file correctly' do
      allow(shellout).to receive(:stdout)
        .and_return('{"test-key":"testvalue","merge-me":{"test-key":"testvalue"}}')

      expect(chef_run).to create_file('/tmp/test').with(
        content: /^testvalue/
      )
    end
  end
  context 'GSM: merge execution' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['merge-me'] = {
          'secrets' => {
            'all-the-secrets' => {
              'backend' => 'gsm',
              'path' => {
                'name' => 'test-secret-chef_env-zero',
                'project' => 'env-zero',
              },
            },
          },
          'test-keys' => {
            'test-key' => 'from-recipe',
            'recipe-key' => 'from-recipe',
          },
        }
        node.normal['secrets'] = {
          'backend' => 'gsm',
          'path' => {
            'name' => 'test-secret-chef_env-zero',
            'project' => 'env-zero',
          },
        }
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      allow(shellout).to receive(:stdout)
        .and_return('{"merge-me":{"test-keys":"testvalue"}}')

      expect { chef_run }.to_not raise_error
    end

    it 'creates the file correctly' do
      allow(shellout).to receive(:stdout)
        .and_return('{"merge-me":{"test-keys":{"test-key":"from-gkms","data-bag-key":"from-gkms"}}}')

      expect(chef_run).to create_file('/tmp/merge-test').with(
        content: '{"test-key"=>"from-gkms", "recipe-key"=>"from-recipe", "data-bag-key"=>"from-gkms"}'
      )
    end
  end
end
