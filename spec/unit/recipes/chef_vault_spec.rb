# Cookbook:: template
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'
require 'chef-vault/test_fixtures'

describe 'gitlab_secrets::_test' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'CHEF_VAULT: default execution' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['merge-me'] = { 'secrets' => {
          'all-the-secrets' => {
            'backend' => 'chef_vault',
            'path' => 'vault',
            'key' => 'merge',
          },
        },
                                    'test-keys' => {
                                      'test-key' => 'from-recipe',
                                      'recipe-key' => 'from-recipe',
                                    },
                           }
        node.normal['secrets'] = {
          'backend' => 'chef_vault',
          'path' => 'vault',
          'key' => 'item',
        }
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the test file' do
      expect(chef_run).to create_file('/tmp/test').with(
        content: /^test-value/
      )
    end
  end

  context 'CHEF_VAULT: merge execution' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['merge-me'] = { 'secrets' => {
          'all-the-secrets' => {
            'backend' => 'chef_vault',
            'path' => 'vault',
            'key' => 'merge',
          },
        },
                                    'test-keys' => {
                                      'test-key' => 'from-recipe',
                                      'recipe-key' => 'from-recipe',
                                    },
                           }
        node.normal['secrets'] = {
          'backend' => 'chef_vault',
          'path' => 'vault',
          'key' => 'item',
        }
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the merge test file' do
      expect(chef_run).to create_file('/tmp/merge-test').with(
        content: '{"test-key"=>"from-data-bag", "recipe-key"=>"from-recipe", "data-bag-key"=>"from-data-bag"}'
      )
    end
  end

  context 'CHEF_VAULT: default execution env based' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['merge-me'] = { 'secrets' => {
          'all-the-secrets' => {
            'backend' => 'chef_vault',
            'path' => 'vault',
            'key' => 'merge',
          },
        },
                                    'test-keys' => {
                                      'test-key' => 'from-recipe',
                                      'recipe-key' => 'from-recipe',
                                    },
                           }
        node.normal['secrets'] = {
          'backend' => 'chef_vault',
          'path' => 'vault',
        }
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the test file' do
      expect(chef_run).to create_file('/tmp/test').with(
        content: /^_default/
      )
    end
  end
end
