# Cookbook:: gitlab_secrets
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'
require 'secrets'

describe 'gitlab_secrets merge_secrets' do
  let(:shellout) do
    double(run_command: nil,
           error!: nil,
           stdout: '',
           stderr: '',
           exitstatus: 0,
           live_stream: '')
  end

  # before do
  #  allow(Mixlib::ShellOut).to receive(:new).and_return(shellout)
  # end

  let(:secret_module) do
    c = Class.new { include Decryptor }
    c.new
  end

  it 'merges simple keys successfully' do
    node = {
      'test' => {
        'mergeable' => {
          'key' => 'node',
        },
        'secrets' => {
          '1' => {
            'backend' => 'gkms',
            'path' => {
              'path' => 'test-secret-chef-env-zero-bucket',
              'item' => 'secret.enc',
            },
            'key' => {
              'ring' => 'test-chef-validation',
              'key' => 'test-validation-pem',
              'location' => 'global',
            },
          },
        },
      },
    }

    allow(Mixlib::ShellOut).to receive(:new)
      .and_return(shellout)

    allow(shellout).to receive(:stdout)
      .and_return('{"test":{"mergeable":{"key":"secret"}}}')

    allow(Chef::Config).to receive(:internal_locale)
      .and_return('en_US.UTF-8')

    allow(secret_module).to receive(:node)
      .and_return(node)

    allow(secret_module).to receive(:cookbook_name)
      .and_return('my_cookbook')

    allow(secret_module).to receive(:recipe_name)
      .and_return('my_recipe')

    expect(secret_module.merge_secrets('test')).to eq(
      'mergeable' => { 'key' => 'secret' },
      'secrets' => {
        '1' => {
          'backend' => 'gkms',
          'path' => {
            'path' => 'test-secret-chef-env-zero-bucket',
            'item' => 'secret.enc',
          },
          'key' => {
            'ring' => 'test-chef-validation',
            'key' => 'test-validation-pem',
            'location' => 'global',
          },
        },
      }
    )
  end

  it 'merges simple hases successfully' do
    node = {
      'test' => {
        'mergeable' => {
          'key' => {
            'node-key' => 'node',
          },
        },
        'secrets' => {
          '1' => {
            'backend' => 'gkms',
            'path' => {
              'path' => 'test-secret-chef-env-zero-bucket',
              'item' => 'secret.enc',
            },
            'key' => {
              'ring' => 'test-chef-validation',
              'key' => 'test-validation-pem',
              'location' => 'global',
            },
          },
        },
      },
    }

    allow(Mixlib::ShellOut).to receive(:new)
      .and_return(shellout)

    allow(shellout).to receive(:stdout)
      .and_return('{"test":{"mergeable":{"key":{"secret-key":"secret"}}}}')

    allow(Chef::Config).to receive(:internal_locale)
      .and_return('en_US.UTF-8')

    allow(secret_module).to receive(:node)
      .and_return(node)

    allow(secret_module).to receive(:cookbook_name)
      .and_return('my_cookbook')

    allow(secret_module).to receive(:recipe_name)
      .and_return('my_recipe')

    expect(secret_module.merge_secrets('test')).to eq(
      'mergeable' => {
        'key' => {
          'node-key' => 'node',
          'secret-key' => 'secret',
        },
      },
      'secrets' => {
        '1' => {
          'backend' => 'gkms',
          'path' => {
            'path' => 'test-secret-chef-env-zero-bucket',
            'item' => 'secret.enc',
          },
          'key' => {
            'ring' => 'test-chef-validation',
            'key' => 'test-validation-pem',
            'location' => 'global',
          },
        },
      }
    )
  end
end
