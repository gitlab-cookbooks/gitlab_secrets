# Cookbook:: gitlab_secrets
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'
require 'secrets'

describe 'gitlab_secrets get_secrets' do
  let(:shellout) do
    double(run_command: nil,
           error!: nil,
           stdout: '',
           stderr: '',
           exitstatus: 0,
           live_stream: '')
  end

  # before do
  #  allow(Mixlib::ShellOut).to receive(:new).and_return(shellout)
  # end

  let(:secret_module) do
    c = Class.new { include Decryptor }
    c.new
  end

  let(:environment_var) do
    # When declaring a :new Mixlib::ShellOut, we need to pass this environment hash
    { environment: {
      'LC_ALL' => 'C.UTF-8',
      'LANGUAGE' => 'C.UTF-8',
      'LANG' => 'C.UTF-8',
      'PATH' => '/builds/gitlab-cookbooks/gitlab_secrets/.bundle/ruby/2.3.0/bin:/usr/local/bundle/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    },
    }
  end
  it 'Fails with unknown backend' do
    path = {
      'name' => 'test-secret-chef_env-zero',
      'project' => 'env-zero',
    }

    allow(secret_module).to receive(:node)
      .and_return({})

    allow(secret_module).to receive(:cookbook_name)
      .and_return('my_cookbook')

    allow(secret_module).to receive(:recipe_name)
      .and_return('my_recipe')

    expect { secret_module.get_secrets('not-known', path, {}) }
      .to raise_error(RuntimeError, 'Secrets backend: not-known - not found (my_cookbook[my_recipe])')
  end
  it 'Fails with missing project' do
    path = {
      'name' => 'test-secret-chef_env-zero',
    }

    allow(secret_module).to receive(:node)
      .and_return({})

    allow(secret_module).to receive(:cookbook_name)
      .and_return('my_cookbook')

    allow(secret_module).to receive(:recipe_name)
      .and_return('my_recipe')

    expect { secret_module.get_secrets('gsm', path, {}) }
      .to raise_error(RuntimeError, 'Path must contain a "name" and a "project", you passed: {"name"=>"test-secret-chef_env-zero"}')
  end

  it 'Fails with missing name' do
    path = {
      'project' => 'env-zero',
    }

    allow(secret_module).to receive(:node)
      .and_return({})

    allow(secret_module).to receive(:cookbook_name)
      .and_return('my_cookbook')

    allow(secret_module).to receive(:recipe_name)
      .and_return('my_recipe')

    expect { secret_module.get_secrets('gsm', path, {}) }
      .to raise_error(RuntimeError, 'Path must contain a "name" and a "project", you passed: {"project"=>"env-zero"}')
  end
end
