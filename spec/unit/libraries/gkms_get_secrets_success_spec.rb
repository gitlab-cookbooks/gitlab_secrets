# Cookbook:: gitlab_secrets
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'
require 'secrets'

describe 'gitlab_secrets::_test' do
  let(:shellout) do
    double(run_command: nil,
           error!: nil,
           stdout: '',
           stderr: '',
           exitstatus: 0,
           live_stream: '')
  end

  # before do
  #  allow(Mixlib::ShellOut).to receive(:new).and_return(shellout)
  # end

  let(:secret_module) do
    c = Class.new { include Decryptor }
    c.new
  end

  let(:environment_var) do
    # When declaring a :new Mixlib::ShellOut, we need to pass this environment hash
    { environment: {
      'LC_ALL' => 'C.UTF-8',
      'LANGUAGE' => 'C.UTF-8',
      'LANG' => 'C.UTF-8',
      'PATH' => '/builds/gitlab-cookbooks/gitlab_secrets/.bundle/ruby/2.3.0/bin:/usr/local/bundle/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    },
    }
  end

  let(:path) do
    {
      'path' => 'test-secret-chef-env-zero-bucket',
      'item' => 'secret.enc',
    }
  end

  before do
    allow(ENV).to receive(:[]).with('PATH').and_return('/usr/local/bundle/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin')
    allow(ENV).to receive(:[]).with('GEM_HOME').and_return('/builds/gitlab-cookbooks/gitlab_secrets/.bundle/ruby/2.3.0/')
    allow(ENV).to receive(:[]).with('GEM_PATH').and_return('/builds/gitlab-cookbooks/gitlab_secrets/.bundle/ruby/2.3.0@global')
    allow(ENV).to receive(:[]).with('GEM_SPEC_CACHE').and_return('')

    allow(shellout).to receive(:stdout)
      .and_return('{"test-key":"testvalue"}')

    allow(Chef::Config).to receive(:internal_locale)
      .and_return('en_US.UTF-8')

    allow(secret_module).to receive(:node)
      .and_return({})

    allow(secret_module).to receive(:cookbook_name)
      .and_return('my_cookbook')

    allow(secret_module).to receive(:recipe_name)
      .and_return('my_recipe')
  end

  it 'converges successfully without a project specified' do
    expected_command = 'gsutil cp gs://test-secret-chef-env-zero-bucket/secret.enc - | gcloud kms decrypt --keyring=test-chef-validation --key=test-validation-pem --location=global --plaintext-file=- --ciphertext-file=- | zcat -f'
    expect(Mixlib::ShellOut).to receive(:new)
      .with(expected_command, environment_var)
      .and_return(shellout)

    key = {
      'ring' => 'test-chef-validation',
      'key' => 'test-validation-pem',
      'location' => 'global',
    }
    expect(secret_module.get_secrets('gkms', path, key)).to eq(
      'test-key' => 'testvalue'
    )
  end

  it 'converges successfully with a project specified' do
    expected_command = 'gsutil cp gs://test-secret-chef-env-zero-bucket/secret.enc - | gcloud --project some-project kms decrypt --keyring=test-chef-validation --key=test-validation-pem --location=global --plaintext-file=- --ciphertext-file=- | zcat -f'
    expect(Mixlib::ShellOut).to receive(:new)
      .with(expected_command, environment_var)
      .and_return(shellout)

    key = {
      'ring' => 'test-chef-validation',
      'key' => 'test-validation-pem',
      'location' => 'global',
      'project' => 'some-project',
    }
    expect(secret_module.get_secrets('gkms', path, key)).to eq(
      'test-key' => 'testvalue'
    )
  end
end
