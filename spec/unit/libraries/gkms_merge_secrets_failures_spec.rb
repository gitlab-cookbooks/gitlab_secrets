# Cookbook:: gitlab_secrets
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'
require 'secrets'

describe 'gitlab_secrets merge_secrets' do
  let(:shellout) do
    double(run_command: nil,
           error!: nil,
           stdout: '',
           stderr: '',
           exitstatus: 0,
           live_stream: '')
  end

  # before do
  #  allow(Mixlib::ShellOut).to receive(:new).and_return(shellout)
  # end

  let(:secret_module) do
    c = Class.new { include Decryptor }
    c.new
  end

  let(:environment_var) do
    # When declaring a :new Mixlib::ShellOut, we need to pass this environment hash
    { environment: {
      'LC_ALL' => 'C.UTF-8',
      'LANGUAGE' => 'C.UTF-8',
      'LANG' => 'C.UTF-8',
      'PATH' => '/builds/gitlab-cookbooks/gitlab_secrets/.bundle/ruby/2.3.0/bin:/usr/local/bundle/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    },
    }
  end
  it 'Fails without the input being a hash' do
    node = {
      'test' => ['secrets'],
    }

    allow(secret_module).to receive(:node)
      .and_return(node)

    allow(secret_module).to receive(:cookbook_name)
      .and_return('my_cookbook')

    allow(secret_module).to receive(:recipe_name)
      .and_return('my_recipe')

    expect { secret_module.merge_secrets('test') }
      .to raise_error(RuntimeError, 'I did not find a valid hash (type: Array): ["secrets"]')
  end
  it 'Fails without a secret key' do
    node = {
      'test' => {
        'mergeable' => {
          'key' => 'node',
        },
        'no-secret' => {
          'a' => {
            'path' => {
              'path' => 'test-secret-chef-env-zero-bucket',
              'item' => 'secret.enc',
            },
            'key' => {
              'ring' => 'test-chef-validation',
              'key' => 'test-validation-pem',
              'location' => 'global',
            },
          },
        },
      },
    }

    allow(secret_module).to receive(:node)
      .and_return(node)

    allow(secret_module).to receive(:cookbook_name)
      .and_return('my_cookbook')

    allow(secret_module).to receive(:recipe_name)
      .and_return('my_recipe')

    expect { secret_module.merge_secrets('test') }
      .to raise_error(RuntimeError, 'There is no `secrets` section in the hash you passed: ["mergeable", "no-secret"]')
  end
  it 'Fails if the secret section has no secrets' do
    node = {
      'test' => {
        'mergeable' => {
          'key' => 'node',
        },
        'secrets' => {
          'name' => 'failure',
        },
      },
    }

    allow(secret_module).to receive(:node)
      .and_return(node)

    allow(secret_module).to receive(:cookbook_name)
      .and_return('my_cookbook')

    allow(secret_module).to receive(:recipe_name)
      .and_return('my_recipe')

    expect { secret_module.merge_secrets('test') }
      .to raise_error(RuntimeError, 'There is not secret section in name, you passed: {"name"=>"failure"} - The key should be a name, and the value should be a secret configuration')
  end
end
