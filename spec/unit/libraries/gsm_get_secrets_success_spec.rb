# Cookbook:: gitlab_secrets
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.
require 'spec_helper'
require 'secrets'

describe 'gitlab_secrets::_test' do
  let(:shellout) do
    double(run_command: nil,
           error!: nil,
           stdout: '',
           stderr: '',
           exitstatus: 0,
           live_stream: '')
  end

  # before do
  #  allow(Mixlib::ShellOut).to receive(:new).and_return(shellout)
  # end

  let(:secret_module) do
    c = Class.new { include Decryptor }
    c.new
  end

  let(:environment_var) do
    # When declaring a :new Mixlib::ShellOut, we need to pass this environment hash
    { environment: {
      'LC_ALL' => 'C.UTF-8',
      'LANGUAGE' => 'C.UTF-8',
      'LANG' => 'C.UTF-8',
      'PATH' => '/builds/gitlab-cookbooks/gitlab_secrets/.bundle/ruby/2.3.0/bin:/usr/local/bundle/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    },
    }
  end

  it 'converges successfully' do
    path = {
      'name' => 'gitlab-omnibus-secrets_test',
      'project' => 'gitlab-test',
    }
    expected_command = 'gcloud --project gitlab-test secrets versions access --secret gitlab-omnibus-secrets_test latest'

    allow(Mixlib::ShellOut).to receive(:new)
      .with(expected_command, environment_var)
      .and_return(shellout)

    allow(ENV).to receive(:[]).with('PATH').and_return('/usr/local/bundle/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin')
    allow(ENV).to receive(:[]).with('GEM_HOME').and_return('/builds/gitlab-cookbooks/gitlab_secrets/.bundle/ruby/2.3.0/')
    allow(ENV).to receive(:[]).with('GEM_PATH').and_return('/builds/gitlab-cookbooks/gitlab_secrets/.bundle/ruby/2.3.0@global')
    allow(ENV).to receive(:[]).with('GEM_SPEC_CACHE').and_return('')

    # allow(Mixlib::ShellOut).to receive(:new)
    #   .with(expected_command)
    #   .and_return(shellout)

    allow(shellout).to receive(:stdout)
      .and_return('{"test-key":"testvalue"}')

    allow(Chef::Config).to receive(:internal_locale)
      .and_return('en_US.UTF-8')

    allow(secret_module).to receive(:node)
      .and_return({})

    allow(secret_module).to receive(:cookbook_name)
      .and_return('my_cookbook')

    allow(secret_module).to receive(:recipe_name)
      .and_return('my_recipe')

    expect(secret_module.get_secrets('gsm', path, {})).to eq(
      'test-key' => 'testvalue'
    )
  end
end
